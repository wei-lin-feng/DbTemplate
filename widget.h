#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QKeyEvent>

#include "Mock/mock.h"
#include "mywidget/mytabelwidget.h"
#include "Tools/settingsfiletool.h"
#include "Tools/configtool.h"

#include "gui/testtable.h"
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void initMock();
protected:
    void keyPressEvent(QKeyEvent *event);  //键盘事件

private:
    void initWidget();  // 初始化控件或界面
    void initTable();   // 初始化表格界面
private:
    Ui::Widget *ui;

    Mock *mock = nullptr;

    // 界面
    SettingsFileTool *settingsFileTool = nullptr;  // 系统配置界面

    // 表格界面
    TestTable *testTable = nullptr;

};
#endif // WIDGET_H
