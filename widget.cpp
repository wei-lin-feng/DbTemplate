#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

//    initMock();
    initWidget();
    initTable();
}

Widget::~Widget()
{
    delete ui;

    if (mock != nullptr)
    {
        delete mock;
    }

    if (settingsFileTool != nullptr)
    {
        delete settingsFileTool;
    }
}

void Widget::initMock()
{
    MyTabelWidget *table = new MyTabelWidget(this);

    ui->stackedWidget->removeWidget(ui->stackedWidget->widget(0));
    ui->stackedWidget->removeWidget(ui->stackedWidget->widget(0));
    ui->stackedWidget->addWidget(table);
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    // 配置隐藏界面
    if ((event->modifiers() == Qt::ControlModifier) && (event->key() == Qt::Key_P))
    {
        if (settingsFileTool != nullptr)
        {
            settingsFileTool->show();
        }
    }
}

void Widget::initWidget()
{
    // stackWidget 去除默认界面
    ui->stackedWidget->removeWidget(ui->stackedWidget->widget(0));
    ui->stackedWidget->removeWidget(ui->stackedWidget->widget(0));
    // 初始化配置界面
    if (settingsFileTool == nullptr)
    {
        settingsFileTool = new SettingsFileTool();
        settingsFileTool->hide();
    }

    // 初始化配置生成
    ConfigTool::getInstance().initConfig();
}

void Widget::initTable()
{
    if (testTable == nullptr)
    {
        testTable = new TestTable(this);
        ui->stackedWidget->addWidget(testTable);
    }
}

