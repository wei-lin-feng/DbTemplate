#ifndef MYLOG_H
#define MYLOG_H

static const char *const LOG_CONF_FILE = "log.conf";
#pragma execution_character_set("utf-8")
#include <QObject>
#include <QCoreApplication>
#include <QFile>

//// easylog 放到 .h 崩溃
class MyLog : public QObject
{
    Q_OBJECT
public:
    explicit MyLog(QObject *parent = nullptr);

    static MyLog& getInstance();

    /// debug
    void debug(QString text);

    /// info
    void info(QString text);
};

#endif // MYLOG_H
