#include "mylog.h"

#include "Log/easylogging++.h"
INITIALIZE_EASYLOGGINGPP

MyLog::MyLog(QObject *parent) : QObject(parent)
{
    // log 配置
    el::Loggers::addFlag(el::LoggingFlag::DisableApplicationAbortOnFatalLog);
    el::Loggers::addFlag(el::LoggingFlag::StrictLogFileSizeCheck);
    //获取日志配置文件路径，输出的文件路径看配置文件
    QString filePath = QCoreApplication::applicationPid() + "/bin/" + QString(LOG_CONF_FILE);
    QFile file(filePath);
    if (!file.exists())
    {
        LOG(DEBUG) << "获取日志配置文件路径失败，文件不存在";
        return ;
    }
    el::Configurations conf(filePath.toStdString());
    el::Loggers::reconfigureAllLoggers(conf);
}

MyLog &MyLog::getInstance()
{
    static MyLog log;
    return log;
}

void MyLog::debug(QString text)
{
    //  转成c的字符串
    LOG(DEBUG)<<text.toStdString();
}

void MyLog::info(QString text)
{
    //  转成c的字符串
    LOG(INFO)<<text.toStdString();
}
