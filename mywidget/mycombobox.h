#ifndef MYCOMBOBOX_H
#define MYCOMBOBOX_H

#include <QWidget>

namespace Ui {
class MyComboBox;
}

class MyComboBox : public QWidget
{
    Q_OBJECT

public:
    explicit MyComboBox(QWidget *parent = nullptr);
    ~MyComboBox();

    QString currentText();

    /// 设置控件状态
    void setEditEnable(bool status);

    /// 设置 combobox 可编辑状态
    void setsetEditable(bool status);

    /// 添加items
    void addItems(QStringList itemList);

    /// 清空
    void clear();
signals:
    void currentTextChange(QString text);
    void currentIndexChange(int index);
    void addOneItem(QString text);
    void removeOneItem(QString text);
private slots:
    void on_btn_add_clicked();

    void on_btn_delete_clicked();

private:
    void initWidget();  // 初始化控件
private:
    Ui::MyComboBox *ui;

    int itemIndex = -1;
};

#endif // MYCOMBOBOX_H
