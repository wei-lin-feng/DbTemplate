#include "mytabelwidget.h"
#include "ui_mytabelwidget.h"

MyTabelWidget::MyTabelWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MyTabelWidget)
{
    ui->setupUi(this);

    initWidget();
}

MyTabelWidget::~MyTabelWidget()
{
    delete ui;
}

void MyTabelWidget::setTableRowCount(int rowCount)
{
    if (table != nullptr)
    {
        table->setRowCount(rowCount);
    }
}

void MyTabelWidget::setTableColumnCount(int columnCount)
{
    if (table != nullptr)
    {
        table->setColumnCount(columnCount);
    }
}

void MyTabelWidget::setHorizontalHeadLabels(QStringList headList)
{
    if (table != nullptr)
    {
        table->setHorizontalHeaderLabels(headList);
    }
}

QString MyTabelWidget::getTableName()
{
    return params.tableName;
}

void MyTabelWidget::initTable(QString tableName)
{
    if (tableName.isEmpty())
    {
        return;
    }

    /// 配置读取
    SettingsParams settings = ConfigTool::getInstance().readSetting(tableName);
    params = Utils::settingsParamsToTableParams(settings, tableName);

    /// 表格初始化
    if (table != nullptr)
    {
        // 列
        table->setColumnCount(params.fieldList.size());

        // 列名称
        QStringList headList;
        for (QString field : params.fieldList)
        {
            headList.append(params.field_name_map.value(field));
        }
        table->setHorizontalHeaderLabels(headList);
        table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    }
}

void MyTabelWidget::initWidget()
{
    //功能按钮栏
    if (functionBar == nullptr)
    {
        functionBar = new FunctionBar(this);
    }
    //table
    if (table == nullptr)
    {
        table = new QTableWidget(this);
    }
    //分页功能区
    if (pageWidget == nullptr)
    {
        pageWidget = new PageWidget(this);
    }
    // 布局
    vbLayout = new QVBoxLayout(this);
    vbLayout->addWidget(functionBar, 1);
    vbLayout->addWidget(table, 20);
    vbLayout->addWidget(pageWidget, 1);
    vbLayout->setMargin(0);

    this->setLayout(vbLayout);
}
