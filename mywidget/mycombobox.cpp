#include "mycombobox.h"
#include "ui_mycombobox.h"

MyComboBox::MyComboBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MyComboBox)
{
    ui->setupUi(this);

    // 初始化控件的配置
    initWidget();
}

MyComboBox::~MyComboBox()
{
    delete ui;
}

QString MyComboBox::currentText()
{
    return ui->comboBox->currentText();
}

void MyComboBox::setEditEnable(bool status)
{
    this->setEnabled(status);
}

void MyComboBox::setsetEditable(bool status)
{
    ui->comboBox->setEditable(status);
}

void MyComboBox::addItems(QStringList itemList)
{
    ui->comboBox->addItems(itemList);
}

void MyComboBox::clear()
{
    ui->comboBox->clear();
}

void MyComboBox::initWidget()
{
    ui->btn_add->setText("+");
    ui->btn_delete->setText("-");
    ui->comboBox->setEditable(true);

    connect(ui->comboBox, QOverload<const QString&>::of(&QComboBox::currentTextChanged), this, &MyComboBox::currentTextChange);
    connect(ui->comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MyComboBox::currentIndexChange);
}

void MyComboBox::on_btn_add_clicked()
{
    // 输入为空判断
    if (ui->comboBox->currentText().isEmpty())
    {
        return;
    }

    if (itemIndex == -1)
    {
        ui->comboBox->addItem(ui->comboBox->currentText());
        ui->comboBox->addItem("");
        itemIndex += 2;
    }
    else
    {
        ui->comboBox->setItemText(itemIndex, ui->comboBox->currentText());
        ui->comboBox->addItem("");
        itemIndex++;
    }

    ui->comboBox->setFocus();

    emit addOneItem(ui->comboBox->currentText());
}

void MyComboBox::on_btn_delete_clicked()
{
    if (itemIndex == -1)
    {
        return;
    }

    // 先发信号再删除，为空值不发信号
    if (ui->comboBox->currentText().isEmpty())
    {
        emit removeOneItem(ui->comboBox->currentText());
    }

    ui->comboBox->removeItem(ui->comboBox->currentIndex());
    itemIndex--;
}
