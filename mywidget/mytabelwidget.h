#ifndef MYTABELWIDGET_H
#define MYTABELWIDGET_H

#include <QWidget>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QHeaderView>

#include "gui/functionbar.h"
#include "gui/pagewidget.h"
#include "DataStruct.h"
#include "Tools/utils.h"
#include "Tools/configtool.h"
namespace Ui {
class MyTabelWidget;
}

class MyTabelWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MyTabelWidget(QWidget *parent = nullptr);
    ~MyTabelWidget();

    //// 设置表格行数
    void setTableRowCount(int rowCount);

    //// 设置表格列数
    void setTableColumnCount(int columnCount);

    //// 设置头部标签
    void setHorizontalHeadLabels(QStringList headList);


    /// 获取表名称
    QString getTableName();

    //// 初始化表格，通过表名称去查配置文件
    void initTable(QString tableName);
public:
    TableParams params;
private:
    void initWidget();
private:
    Ui::MyTabelWidget *ui;
    QTableWidget *table = nullptr;       // 表格
    FunctionBar *functionBar = nullptr;// 功能按钮栏
    PageWidget *pageWidget = nullptr;  // 分页功能区
    QVBoxLayout *vbLayout;              //垂直布局
};

#endif // MYTABELWIDGET_H
