#ifndef SETTINGSERVER_H
#define SETTINGSERVER_H

#include <QObject>
#include "Tools/configtool.h"
#include "Tools/dbtool.h"
#include "DataStruct.h"
class SettingServer : public QObject
{
    Q_OBJECT
public:
    explicit SettingServer(QObject *parent = nullptr);

    /// 获取数据库类型
    QStringList getDbTypeList();

    /// 获取数据类型映射
    QMap<QString, QString> getDataTypeMap();

    /// 获取数据库连接
    bool getConnect(DbParams &params);

    /// 表格配置写入配置文件中
    void writeTableParamsToSettings(TableParams params);

    /// 创建表格
    bool createTable(TableParams params);
signals:

public slots:
};

#endif // SETTINGSERVER_H
