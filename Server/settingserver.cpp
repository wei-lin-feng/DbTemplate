#include "settingserver.h"

SettingServer::SettingServer(QObject *parent) : QObject(parent)
{

}

QStringList SettingServer::getDbTypeList()
{
    // 获取参数
    SettingsParams params = ConfigTool::getInstance().readSetting("DATABASE");

    // 截取type list
    QString value = params.settingsMap.value("DATABASE").value("DB_TYPE_MAP");
    QMap<QString,QString> map = Utils::stringToMap(value);

    return map.values();
}

QMap<QString, QString> SettingServer::getDataTypeMap()
{
    // 获取参数
    SettingsParams params = ConfigTool::getInstance().readSetting("DATABASE");

    // 截取type list
    QString value = params.settingsMap.value("DATABASE").value("DB_TYPE_MAP");
    QMap<QString,QString> map = Utils::stringToMap(value);

    return map;
}

bool SettingServer::getConnect(DbParams &params)
{
    return DbTool::getInstance().getOneDbConnect(params);
}

void SettingServer::writeTableParamsToSettings(TableParams params)
{
    SettingsParams config = Utils::tableParamsToSettingParams(params);
    ConfigTool::getInstance().writeSettings(config);
}

bool SettingServer::createTable(TableParams params)
{
    bool status = DbTool::getInstance().creatTable(params);
    if (!status)
    {
        MyLog::getInstance().debug(DbTool::getInstance().getLastErrorInfo());
    }
    return status;
}

