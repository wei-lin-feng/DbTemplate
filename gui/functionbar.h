#ifndef FUNCTIONBAR_H
#define FUNCTIONBAR_H

#include <QWidget>

namespace Ui {
class FunctionBar;
}

class FunctionBar : public QWidget
{
    Q_OBJECT

public:
    explicit FunctionBar(QWidget *parent = nullptr);
    ~FunctionBar();

private:
    Ui::FunctionBar *ui;
};

#endif // FUNCTIONBAR_H
