#include "functionbar.h"
#include "ui_functionbar.h"

FunctionBar::FunctionBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FunctionBar)
{
    ui->setupUi(this);
}

FunctionBar::~FunctionBar()
{
    delete ui;
}
