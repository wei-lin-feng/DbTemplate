QT       += core gui
QT       += sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Log/easylogging++.cc \
    Log/mylog.cpp \
    Mock/mock.cpp \
    Server/settingserver.cpp \
    Tools/configtool.cpp \
    Tools/dbtool.cpp \
    Tools/settings/createdbdialog.cpp \
    Tools/settingsfiletool.cpp \
    Tools/utils.cpp \
    Tools/wlfconnecttool.cpp \
    gui/functionbar.cpp \
    gui/pagewidget.cpp \
    gui/testtable.cpp \
    main.cpp \
    mywidget/mycombobox.cpp \
    mywidget/mytabelwidget.cpp \
    widget.cpp

HEADERS += \
    Dao/settingdao.h \
    DataStruct.h \
    Log/easylogging++.h \
    Log/mylog.h \
    Mock/mock.h \
    Server/settingserver.h \
    Tools/configtool.h \
    Tools/dbtool.h \
    Tools/settings/createdbdialog.h \
    Tools/settingsfiletool.h \
    Tools/utils.h \
    Tools/wlfconnecttool.h \
    gui/functionbar.h \
    gui/pagewidget.h \
    gui/testtable.h \
    mywidget/mycombobox.h \
    mywidget/mytabelwidget.h \
    widget.h

FORMS += \
    Tools/settings/createdbdialog.ui \
    Tools/settingsfiletool.ui \
    gui/functionbar.ui \
    gui/pagewidget.ui \
    mywidget/mycombobox.ui \
    mywidget/mytabelwidget.ui \
    widget.ui

MOC_DIR = temp/moc
RCC_DIR = temp/rcc
UI_DIR = temp/ui
OBJECTS_DIR = temp/ob

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
