#ifndef DATASTRUCT_H
#define DATASTRUCT_H

static const char *const DBCONFIG_FILE = "databaseConfig.ini";
static const char *const SQLITEDB_FILE = "DbData.db";
static const char *const TABLE_CONFIG_KEY = "params";
#include <QString>
#include <QMap>
#include <QSqlDatabase>
enum GROUP_ID
{
    TABLE_CONFIG,  // 表配置
    DB_CONFIG      // 数据库配置
};

enum DBTYPE
{
    SQLITE,
    MYSQL
};

//// 配置存储形式 (a1,a2...);(b1,b2...)
struct InitConfig
{
    QStringList groupList;
    QMap<QString, QMap<QString, QStringList>> params;

    InitConfig()
    {

    }
};

struct TableConfig
{
    QString tableName;  // 表名
    int rowCount;       // 行计数
    QString fieldList;  // 字段列表，map可能乱序，用list保证顺序
    QMap<QString,QString> field_name_map;   // 字段-名称映射   key : 字段  value : 字段名
    QMap<QString,QString> field_type_map;   // 字段-数据类型映射 key : 字段  value : 字段数据类型
    QMap<QString,QMap<QString, QString>> dictMap; // 字典映射  <字段，<键，值>>

    bool status; //配置读取状态
    TableConfig()
    {
        rowCount = 50;
    }
};

/// 配置写入结构体
struct InserSettingParams
{
    // 组名
    QString groupName;
    // 键值对
    QMap<QString,QString> data;
    //    SettingsParams()
    //    {
    //        groupList.append("DATABASE");

    //        // 数据库配置
    //        settingsMap["DATABASE"]["TYPE"] = "SQLITE";
    //        settingsMap["DATABASE"]["DB_NAME"] = "";
    //        settingsMap["DATABASE"]["HOST_IP"] = "127.0.0.1";
    //        settingsMap["DATABASE"]["PORT"] = "3306";
    //        settingsMap["DATABASE"]["USER"] = "root";
    //        settingsMap["DATABASE"]["PASS_WORD"] = "123456";
    //    }

};

/// 配置读取结构体
struct SettingsParams
{
    // 组名列表
    QStringList groupList;
    // 组名、键、值
    QMap<QString, QMap<QString, QString>> settingsMap;

    // 配置读取状态
    bool readSuccess;
    QString errorInfo;
    QString currentGroupName;  // 读取的标志保存 （为哪个group....）
};

/// 数据库连接参数
struct DbParams
{
    int dbType;        // 数据库类型
    QString hostName;  //主机名称
    int port;          // 端口
    QString dbName;    // 数据库名称
    QString connectName; // 连接名称
    QSqlDatabase db;    // 返回的数据库连接

    DbParams()
    {
        dbType = SQLITE;
        hostName = "127.0.0.1";
        port = 3306;
        dbName = "";
        connectName = "";
    }
};

/// 表格创建参数结构体
struct TableParams
{
    QString tableName;          // 表名称
    QString primaryKeyField;    // 主键字段
    QStringList fieldList;      // 字段列表,保证创建时的顺序
    QMap<QString, QString> field_type_map; // 字段_类型映射
    QMap<QString, QString> field_name_map;  // 字段_名称映射
    QMap<QString, QString> typeChangeMap;  // 数据类型转换map
    QString lastErrorInfo;  // 错误信息

    TableParams()
    {
    }

    /**
     * 字段添加
     * @param field 字段
     * @param fieldName 字段名称
     * @param type 字段数据类型
     * @return
     */
    bool addField(QString field, QString fieldName, QString dataType)
    {
        // 空值判断
        if (field.isEmpty() || dataType.isEmpty() || fieldName.isEmpty())
        {
            lastErrorInfo = " fieldName or dataType is empty at TableCreateParams::addField ";
            return false;
        }
       // 数据类型判断
        if (!typeChangeMap.values().contains(dataType) && !typeChangeMap.isEmpty())
        {
            lastErrorInfo = " dataType is no exist , please add to typeChangeMap or check dataType";
            return false;
        }
        // 重复添加判断
        if (fieldList.contains(field))
        {
            return true;
        }

        fieldList.append(field);
        field_type_map.insert(field, dataType);
        field_name_map.insert(field, fieldName);
    }

    /**
     * 获取数据类型
     * @param field 字段
     * @return
     */
    QString getFieldType(QString field)
    {
        return field_type_map.value(field);
    }
};
#endif // DATASTRUCT_H
