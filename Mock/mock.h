#ifndef MOCK_H
#define MOCK_H

#include <QObject>
#include <QStackedWidget>

#define MOCK 1;
class Mock : public QObject
{
    Q_OBJECT
public:
    explicit Mock(QObject *parent = nullptr);
    ~Mock();

    // 测试主界面
    void setStackedWidget(QStackedWidget *widget);
signals:

private:
};

#endif // MOCK_H
