#include "settingsfiletool.h"
#include "ui_settingsfiletool.h"
#include "Log/mylog.h"
SettingsFileTool::SettingsFileTool(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsFileTool)
{
    ui->setupUi(this);

    // 初始化界面
    initWidget();

    // 初始化交互层
    initServer();
}

SettingsFileTool::~SettingsFileTool()
{
    db.close();
    delete ui;

    if (createDbDialog != nullptr)
    {
        delete createDbDialog;
    }

    if (layout != nullptr)
    {
        delete  layout;
    }

    for (int key : tableManageMap.keys())
    {
        tableManageMap[key]->clearContents();
        delete tableManageMap[key];
    }
}

void SettingsFileTool::initWidget()
{
    // 表格界面初始化配置
    initTab_TableConfig();

    // 初始化 sqlite 配置界面
    initTab_SqliteConfig();
}

void SettingsFileTool::initServer()
{
    settingServer = new SettingServer(this);
}

void SettingsFileTool::initTab_TableConfig()
{
    ui->comboBox->setEditable(true);

    // 表格
    ui->tableWidget->setRowCount(1);
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void SettingsFileTool::initTab_SqliteConfig()
{
    // 去除默认的页
    ui->tabWidget_Table->removeTab(0);
    ui->tabWidget_Table->removeTab(0);

    //自定义 cbb
    if (mycbb == nullptr)
    {
        mycbb = new MyComboBox(this);
        layout = new  QVBoxLayout();
        layout->addWidget(mycbb);
        ui->MyComboBox->setLayout(layout);

        connect(mycbb, &MyComboBox::addOneItem, this, &SettingsFileTool::on_addTab);
        connect(mycbb, &MyComboBox::removeOneItem, this, &SettingsFileTool::on_removeTable);
        connect(mycbb, &MyComboBox::currentTextChange, this, [=](QString text){
            if (!name_id_map.keys().contains(text))
            {
                return;
            }
            else
            {
                int id = name_id_map.value(text);
                ui->tabWidget_Table->setCurrentIndex(id);
            }
        });
    }

    // tabwidget 添加右键菜单
    ui->tabWidget_Table->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tabWidget_Table, &QTableWidget::customContextMenuRequested, this, &SettingsFileTool::tableWidgetRightKeyBtnMenu);
}

bool SettingsFileTool::sqliteOpenByPath(QString path, QString connectName)
{
    DbParams params;
    params.dbType = SQLITE;
    params.dbName = path;

    if (!connectName.isEmpty())
    {
        params.connectName = connectName;
    }

    if (!DbTool::getInstance().getOneDbConnect(params))
    {
        QMessageBox::information(this, "提示", QString("数据库打开失败：%1").arg(params.db.lastError().text()));
        MyLog::getInstance().debug(QString("数据库打开失败：%1").arg(params.db.lastError().text()));
        qDebug()<<QString("数据库打开失败：%1").arg(params.db.lastError().text());
        return false;
    }
    else
    {
        // 拿到数据库连接
        db = params.db;
        QMessageBox::information(this, "提示", QString("数据库打开成功"));
        return true;
    }
}

void SettingsFileTool::on_btn_table_clicked()
{
    TableConfig config;
    // 单页行数
    if (!ui->le_rowCount->text().isEmpty())
    {
        config.rowCount = ui->le_rowCount->text().toInt();
    }
    // 表名
    if (!ui->comboBox->currentText().isEmpty())
    {
        config.tableName = ui->comboBox->currentText();
    }
}

void SettingsFileTool::on_btn_createDb_clicked()
{
    if (createDbDialog == nullptr)
    {
        createDbDialog = new CreateDbDialog();
        connect(createDbDialog, &CreateDbDialog::sendDbPath, this, [=](QString path){sqliteOpenByPath(path);});
    }

    createDbDialog->show();
}

//// 数据表创建分两个步骤：1.生成参数输入界面 2.点击创建表按钮读取、保存参数
void SettingsFileTool::on_addTab(QString text)
{
    if (!db.isOpen())
    {
        QMessageBox::information(this, "提示", "数据库未打开");
        return;
    }

    if (mycbb == nullptr)
    {
        QMessageBox::information(this, "提示", "控件未加载");
        return;
    }

    if (text.isEmpty())
    {
        QMessageBox::information(this, "提示", "表名为空");
        return;
    }

    if (name_id_map.keys().contains(text))  //表已存在
    {
        return;
    }

    // 表格参数输入界面创建
    // tablewidget创建
    int key = ui->tabWidget_Table->count();
    tableManageMap[key] = new QTableWidget();
    tableManageMap[key]->setColumnCount(3);
    tableManageMap[key]->setRowCount(1);
    QStringList head;
    head << "字段" << "字段名" << "字段类型";
    tableManageMap[key]->setHorizontalHeaderLabels(head);
    name_id_map.insert(text, key);
    tableManageMap[key]->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    // 添加 QTableWidgetItem,否则获取操作都会为空值
    for (int row = 0; row < key+1; row++)
    {
        for (int column = 0; column < 3; column++)
        {
            tableManageMap[key]->setItem(row, column, new QTableWidgetItem());
        }

        // 初始化数据类型cbb
        QComboBox *cbb = new QComboBox(this);
        cbb->addItems(settingServer->getDbTypeList());
        tableManageMap[key]->setCellWidget(row, 2, cbb);
//        cbbManageMap[key][row] = cbb;
    }
    // 添加tab
    ui->tabWidget_Table->addTab(tableManageMap.value(key), text);
}

void SettingsFileTool::on_removeTable(QString text)
{
    if (text.isEmpty())
    {
        QMessageBox::information(this, "提示", "表名为空");
        return;
    }

    // 获取界面下标
    int id = name_id_map.value(text);
    // 移除界面
    ui->tabWidget->removeTab(id);
    // 管理map 清除记录
    delete tableManageMap[id];
    tableManageMap.remove(id);
    name_id_map.remove(text);
}

void SettingsFileTool::on_btn_open_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, "打开sqlite文件", QCoreApplication::applicationDirPath(), "sqlite file (*.db)");
    if (filePath.isEmpty())
    {
        return;
    }

    ui->lineEdit->setText(filePath);

    if (!db.isOpen())
    {
        DbParams params;
        params.dbType = SQLITE;
        params.dbName = filePath;
        params.connectName = "MAIN";

    }

    // 打开成功，获取全部表格名称
    QStringList tableNameList = db.tables();
    if (tableNameList.isEmpty())
    {
        MyLog::getInstance().debug(DbTool::getInstance().getLastErrorInfo());
        return;
    }

    // 添加item
    if (mycbb == nullptr)
    {
        return;
    }
    tableNameList.append("");
    mycbb->addItems(tableNameList);

    // 创建 tab 界面
    for (QString tableName : tableNameList)
    {
        if (tableName.isEmpty())
        {
            continue;
        }
        on_addTab(tableName);
    }

    // 获取数据库信息

}

void SettingsFileTool::on_btn_createTable_clicked()
{
    /// 参数获取
    QTableWidget* table = getCurrentTable();
    int rowCount = table->rowCount();

    QString tableName = mycbb->currentText();
    QStringList fieldList;
    QStringList fieldNameList;
    QStringList typeList;
    for (int row = 0; row < rowCount; row++)
    {
        fieldList.append(table->item(row, 0)->text());
        fieldNameList.append(table->item(row, 1)->text());
        QComboBox *cbb = (QComboBox*)table->cellWidget(row, 2);
        typeList.append(cbb->currentText());
    }


    /// 参数拼接
    TableParams tableParams;
    tableParams.tableName = tableName;
    tableParams.typeChangeMap = settingServer->getDataTypeMap();
    for (int i = 0; i < fieldList.size(); i++)
    {
        if (!tableParams.addField(fieldList.at(i),fieldNameList.at(i),typeList.at(i)))
        {
            MyLog::getInstance().debug(tableParams.lastErrorInfo);
        }
    }
    // 写入配置文件
    settingServer->writeTableParamsToSettings(tableParams);
    // 表格生成
    QString str = settingServer->createTable(tableParams) == true?"成功":"失败";
    QMessageBox::information(this, "提示", QString("数据表创建:%1").arg(str));
}

void SettingsFileTool::on_actionRemoveSelectRow_triggered()
{
    // 状态判断
    if (tableManageMap.isEmpty())
    {
        return;
    }
    // 选中判断
    // 获取当前表格指针
    QTableWidget *table = getCurrentTable();
    if (table == nullptr)
    {
        return;
    }
    // 获取选中行
    QList<QTableWidgetItem*> items = table->selectedItems();
    int rowCount = table->rowCount();
    qDebug()<<" row count :"<<table->rowCount();
    if (items.isEmpty())
    {
        return ;
    }
    int selectRow = items.first()->row();


    //判断
    QMessageBox message(QMessageBox::NoIcon,
                        "删除", "你确定要删除吗?",
                        QMessageBox::Yes | QMessageBox::No, this);
    int res = message.exec();
    if (res == QMessageBox::Yes)
    {
        // 表格移除  删除行会自动释放item
        table->removeRow(selectRow);
    }
}

void SettingsFileTool::on_actionAddRow_triggered()
{
    // 状态判断
    if (tableManageMap.isEmpty())
    {
        return;
    }
    QTableWidget *table = getCurrentTable();
    if (table == nullptr)
    {
        return;
    }
    table->insertRow(table->rowCount());


    // 添加 QTableWidgetItem,否则获取操作都会为空值
    int rowIndex = table->rowCount() - 1;
    for (int column = 0; column < 3; column++)
    {
        table->setItem(rowIndex, column, new QTableWidgetItem());
    }


    // 初始化数据类型cbb
    QComboBox *cbb = new QComboBox(this);
    cbb->addItems(settingServer->getDbTypeList());
    table->setCellWidget(rowIndex, 2, cbb);
}

void SettingsFileTool::tableWidgetRightKeyBtnMenu(const QPoint &pos)
{
    if (menu == nullptr)
    {
        menu = new QMenu(ui->tabWidget_Table);
        removeSelectRow = new QAction("删除选中", ui->tabWidget_Table);
        addRow = new QAction("添加行", ui->tabWidget_Table);

        menu->addAction(removeSelectRow);
        menu->addAction(addRow);

        connect(removeSelectRow, &QAction::triggered, this, &SettingsFileTool::on_actionRemoveSelectRow_triggered);
        connect(addRow, &QAction::triggered, this, &SettingsFileTool::on_actionAddRow_triggered);
    }

    menu->exec(QCursor::pos());
}

QTableWidget *SettingsFileTool::getCurrentTable()
{
    int key = ui->tabWidget_Table->currentIndex();

    return tableManageMap.value(key);
}
