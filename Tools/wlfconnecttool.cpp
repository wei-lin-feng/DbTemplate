#include "wlfconnecttool.h"

WLFConnectTool::WLFConnectTool(QObject *parent) : QObject(parent)
{

}

WLFConnectTool::~WLFConnectTool()
{
    // 清空监听列表
    for (QString key : connecterMap.keys())
    {
        slotDetach(key);
    }
}

WLFConnectTool &WLFConnectTool::getInstance()
{
    static WLFConnectTool tool;
    return tool;
}

void WLFConnectTool::slotAttach(QString funcName, QObject *receive, const char *slot)
{
    Observe *ob = new Observe();
    connect(ob, SIGNAL(notify), receive, SLOT(slot));

    // 重绑定判断
    if (connecterMap.keys().contains(funcName))
    {
        // 先移除
        slotDetach(funcName);
    }

    // 添加到管理map
    connecterMap.insert(funcName, ob);
}

void WLFConnectTool::slotDetach(QString funcName)
{
    if (connecterMap.keys().contains(funcName))
    {
        delete connecterMap.value(funcName);
        connecterMap.remove(funcName);
    }
}

void WLFConnectTool::nofity(QString funcName)
{
    if (connecterMap.keys().contains(funcName))
    {
        emit connecterMap.value(funcName)->notify();
    }
}
