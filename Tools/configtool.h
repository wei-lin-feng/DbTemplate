#ifndef CONFIGTOOL_H
#define CONFIGTOOL_H
#pragma execution_character_set("utf-8")
#include <QObject>
#include <QCoreApplication>
#include <QSettings>
#include <QtSql/QSqlDatabase>
#include <QSqlError>
#include <QDir>
#include <QTextCodec>
#include "DataStruct.h"

#include "utils.h"
class ConfigTool : public QObject
{
    Q_OBJECT
public:
    explicit ConfigTool(QObject *parent = nullptr);

    /// 获取单例
    static ConfigTool& getInstance();
    /// 获取表格配置
    TableConfig getTableConfig();
    /// 获取配置文件路径
    QString getSettingsFilePath();
    /// 获取SQLITE文件路径
    QString getSqliteFilePath();

    /// 部分配置读取
    SettingsParams readSetting(QString groupName);
    /// 读取全部配置
    SettingsParams readAllSetting();
    /// 写入配置
    void writeSettings(SettingsParams params);

    /// 写入表格配置
    void writeTableConfig(TableConfig config);

    /// 初始化部分配置
    void initConfig();
signals:

private:
    SettingsParams settingsParams;  // 配置参数
};

#endif // CONFIGTOOL_H
