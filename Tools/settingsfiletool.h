#ifndef SETTINGSFILETOOL_H
#define SETTINGSFILETOOL_H
#pragma execution_character_set("utf-8")
#include <QWidget>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlError>
#include <QDebug>
#include <QVBoxLayout>
#include <QTableWidget>
#include <QAction>
#include <QMenu>
#include <QComboBox>

#include "DataStruct.h"
#include "Tools/settings/createdbdialog.h"
#include "Tools/dbtool.h"
#include "mywidget/mycombobox.h"
#include "Server/settingserver.h"
namespace Ui {
class SettingsFileTool;
}

class SettingsFileTool : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsFileTool(QWidget *parent = nullptr);
    ~SettingsFileTool();

private slots:
    void on_btn_table_clicked();

    void on_btn_createDb_clicked();

private:
    void initWidget();  // 初始化界面
    void initServer(); // 初始化操作层（对下）

    void initTab_TableConfig();  // 初始化表格配置界面
    void initTab_SqliteConfig(); // 初始化sqlite 配置界面

private slots:
    bool sqliteOpenByPath(QString path, QString connectName = "");
    void on_addTab(QString text); // 新建表
    void on_removeTable(QString text); // 删除表

    void on_btn_open_clicked();

    void on_btn_createTable_clicked();

private:
    QTableWidget *getCurrentTable();
    void on_actionRemoveSelectRow_triggered();
    void on_actionAddRow_triggered();
    void tableWidgetRightKeyBtnMenu(const QPoint &pos);
private:
    Ui::SettingsFileTool *ui;

    // sqlite 创建窗口
    CreateDbDialog *createDbDialog = nullptr;
    QSqlDatabase db;
    MyComboBox *mycbb = nullptr;  // 自定义的cbb
    QVBoxLayout *layout;

    // 动态生成管理 key : page id
    QMap<int, QTableWidget*> tableManageMap;
    QMap<QString, int> name_id_map;  // tab 名称-id map
    QMap<int,QMap<int, QComboBox*>> cbbManageMap;

    // 表格右键菜单
    QMenu *menu = nullptr;
    QAction *addRow = nullptr;
    QAction *removeSelectRow = nullptr;

    // 配置界面数据交互操作层
    SettingServer *settingServer = nullptr;
};

#endif // SETTINGSFILETOOL_H
