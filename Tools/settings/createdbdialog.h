#ifndef CREATEDBDIALOG_H
#define CREATEDBDIALOG_H
#pragma execution_character_set("utf-8")
#include <QDialog>
#include <QFileDialog>
#include <QCoreApplication>
#include <QMessageBox>


namespace Ui {
class CreateDbDialog;
}

class CreateDbDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateDbDialog(QWidget *parent = nullptr);
    ~CreateDbDialog();
signals:
    void sendDbPath(QString fileName);
private slots:
    void on_btn_sure_clicked();

    void on_btn_open_clicked();

    void on_btn_cancel_clicked();

private:
    Ui::CreateDbDialog *ui;
};

#endif // CREATEDBDIALOG_H
