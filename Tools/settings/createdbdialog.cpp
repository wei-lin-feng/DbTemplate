#include "createdbdialog.h"
#include "ui_createdbdialog.h"
#include <QDebug>
#include "Log/mylog.h"
CreateDbDialog::CreateDbDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateDbDialog)
{
    ui->setupUi(this);

    // 去除问号
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
}

CreateDbDialog::~CreateDbDialog()
{
    delete ui;
}

void CreateDbDialog::on_btn_sure_clicked()
{
    if (ui->le_name->text().isEmpty())
    {
        QMessageBox::information(this, "提示", "请输入数据库名称");
        return;
    }

    if (ui->le_dir->text().isEmpty())
    {
        QMessageBox::information(this, "提示", "请选择存储路径");
        return;
    }

    QString filePath = ui->le_dir->text() + "/" + ui->le_name->text() + ".db";
    MyLog::getInstance().debug(filePath);
    qDebug()<<filePath;

    emit sendDbPath(filePath);
    this->close();
}

void CreateDbDialog::on_btn_open_clicked()
{
    QString path = QCoreApplication::applicationDirPath();
    QString dirPath = QFileDialog::getExistingDirectory(this, "选择目录", path);

    ui->le_dir->setText(dirPath);
}

void CreateDbDialog::on_btn_cancel_clicked()
{
    this->close();
}
