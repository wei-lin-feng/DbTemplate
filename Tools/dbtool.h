#ifndef DBTOOL_H
#define DBTOOL_H
#pragma execution_character_set("utf-8")
#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMap>

#include "Log/mylog.h"
#include "DataStruct.h"

class DbTool : public QObject
{
    Q_OBJECT
public:
    explicit DbTool(QObject *parent = nullptr);
    ~DbTool();

    static DbTool& getInstance();

    /**
     * 设置连接参数
     * @param params 数据库参数
     * @return
     */
    void setConnectParams(DbParams params);

    /**
     * 获取一个数据库连接
     * @param params 数据库参数
     * @return
     */
    bool getOneDbConnect(DbParams &params);

    /**
     * 检索数据库全部表格
     * @param db 数据库连接
     * @return
     */
    QStringList getAllTableName();

    /**
     * 获取错误信息
     * @return
     */
    QString getLastErrorInfo();

    /**
     * 数据表创建
     * @param tableCreateParams 表创建结构体
     * @return
     */
    bool creatTable(TableParams tableCreateParams);

signals:

private:

private:
    QMap<QString, QSqlDatabase> connectManage;
    int connectId = 0;
    QString keyTemp = "connect%1";

    // 默认连接
    QSqlDatabase mainConnect;

    // 错误信息保存
    QString lastErrorInfo;

    // 全局连接
    QSqlDatabase db;
};

#endif // DBTOOL_H
