#ifndef WLFCONNECTTOOL_H
#define WLFCONNECTTOOL_H

#include <QObject>
#include <QMap>


//// 实际信号连接对象类 观察者
class Observe : public QObject
{
    Q_OBJECT
public:
    Observe(){};
signals:
    void notify();
};



//// 信号连接绑定工具
class WLFConnectTool : public QObject
{
    Q_OBJECT
public:
    explicit WLFConnectTool(QObject *parent = nullptr);
    ~WLFConnectTool();

    /**
     * 获取连接工具类单例
     * @return
     */
    static WLFConnectTool &getInstance();

    /**
     * 无参数槽绑定
     * @param funcName 成员方法名称，最好加上阈限定，以防止重名
     * @param receive 槽函数对象
     * @param slot 槽函数
     * @return
     */
    void slotAttach(QString funcName, QObject *receive, const char* slot);

    /**
     * 槽解绑
     * @param funcName 成员方法名称，最好加上阈限定，以防止重名
     * @return
     */
    void slotDetach(QString funcName);

    /**
     * 触发信号
     * @param funcName 成员方法名称，最好加上阈限定，以防止重名
     * @return
     */
    void nofity(QString funcName);
signals:

private:
    QMap<QString, Observe*> connecterMap; //监听map
};

#endif // WLFCONNECTTOOL_H
