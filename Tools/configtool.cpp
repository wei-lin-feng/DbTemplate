#include "configtool.h"

#include "Log/mylog.h"
ConfigTool::ConfigTool(QObject *parent) : QObject(parent)
{

}

ConfigTool &ConfigTool::getInstance()
{
    static ConfigTool config;
    return config;
}

TableConfig ConfigTool::getTableConfig()
{

}

QString ConfigTool::getSettingsFilePath()
{
    // bin 目录存在判断
    QString dirPath = QCoreApplication::applicationDirPath() + "/bin";
    QDir dir;
    if (!dir.exists(dirPath))
    {
        if (!dir.mkdir(dirPath))
        {
            MyLog::getInstance().debug("dir create falid");
        }
    }
    QString filePath = dirPath + "/" + QString(DBCONFIG_FILE);
    return filePath;
}

QString ConfigTool::getSqliteFilePath()
{
    // 数据库会生成，不需要加为空判断
    return QCoreApplication::applicationDirPath() + "/Data/" + QString(SQLITEDB_FILE);
}

SettingsParams ConfigTool::readSetting(QString groupName)
{
    // 获取 QSettings
    SettingsParams params;
    QString path = getSettingsFilePath();
    QSettings settings(path, QSettings::IniFormat);
    settings.setIniCodec(QTextCodec::codecForName("UTF-8"));
    QStringList groups =  settings.childGroups();
    // 判断是否存在分组
    if (!groups.contains(groupName))
    {
        params.readSuccess = false;
        params.errorInfo = QString(" groupName %1 is no exist at read settings .").arg(groupName);
        return params;
    }
    // 读取数据
    params.currentGroupName = groupName;
    settings.beginGroup(groupName);
    QStringList allKeys = settings.allKeys();
    for (QString key : allKeys)
    {
        params.settingsMap[groupName][key] = settings.value(key).toString();
    }
    params.readSuccess = true;
    return params;
}

SettingsParams ConfigTool::readAllSetting()
{
    SettingsParams params;
    QString path = getSettingsFilePath();
    QSettings settings(path, QSettings::IniFormat);
    settings.setIniCodec(QTextCodec::codecForName("utf-8"));

    QStringList groupList = settings.childGroups();
    for (QString group : groupList)
    {
        params.groupList.append(group);

        settings.beginGroup(group);
        QStringList keyList = settings.allKeys();
        for (QString key : keyList)
        {
            QString value = settings.value(key).toString();
            params.settingsMap[group][key] = value;
        }
        settings.endGroup();
    }

    return params;
}

void ConfigTool::writeSettings(SettingsParams params)
{
    QString path = getSettingsFilePath();
    QSettings settings(path, QSettings::IniFormat);
    settings.setIniCodec(QTextCodec::codecForName("utf-8"));

    for (QString group : params.groupList)
    {
        settings.beginGroup(group);
        for (QString key : params.settingsMap.value(group).keys())
        {
            QString value = params.settingsMap.value(group).value(key);
            settings.setValue(key, value);
        }
        settings.endGroup();
    }
}

void ConfigTool::writeTableConfig(TableConfig config)
{
    QString filePath = getSettingsFilePath();
    QSettings settings(filePath, QSettings::IniFormat);
    QString groupName = settingsParams.groupList.at(TABLE_CONFIG);
    settings.beginGroup(groupName);

    // 写入单页行数
    settings.setValue("rowCount", config.rowCount);

    // 写入表格配置 不做空数据判断，由上一层过滤
    // 配置字段拼接
    for (QString field : config.fieldList)
    {

    }
}

// 固定配置的初始化
void ConfigTool::initConfig()
{
    //// 先读取旧配置
    SettingsParams params = readAllSetting();
    //// 参数拼接
    /// 数据库参数
    params.groupList.append("DATABASE");
    // 数据库类型映射   int,int;short,smailint....   整合成QString
    QMap<QString, QString> type_map;
    type_map.insert("int","int");
    type_map.insert("float","float");
    type_map.insert("double","double");
    type_map.insert("short","smallint");
    type_map.insert("unsigned int","int");
    type_map.insert("unsigned float","float");
    type_map.insert("unsigned double","double");
    type_map.insert("unsigned short","smallint");
    type_map.insert("QString","varchar(255)");
    type_map.insert("qint64","bigint(20)");
    type_map.insert("long long","bigint(20)");
    QString value = Utils::mapToString(type_map);
    params.settingsMap["DATABASE"]["DB_TYPE_MAP"] = value;


    //// 写入配置
    writeSettings(params);
}

