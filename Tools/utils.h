#ifndef UTILS_H
#define UTILS_H
#pragma once
#include <QString>
#include <QFileInfo>
#include <QMap>

#include "DataStruct.h"
class Utils
{
public:
    Utils();

    /// 从绝对路径获取文件名称
    static QString getFileNameFromPath(QString path);

    /// 结构转换 qmap 转换成固定格式QString
    static QString mapToString(QMap<QString, QString> map);
    /// 结构转换 固定格式QString 转换成 qmap
    static QMap<QString, QString> stringToMap(QString value);
    /// 结构转换 表格配置结构体转换成为通用配置结构体
    static SettingsParams tableParamsToSettingParams(TableParams params);
    /// 结构转换 通用配置结构体转换成为表格配置结构体
    static TableParams settingsParamsToTableParams(SettingsParams params, QString tableName);
};

#endif // UTILS_H
