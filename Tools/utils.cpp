#include "utils.h"

Utils::Utils()
{

}

QString Utils::getFileNameFromPath(QString path)
{
    QFileInfo info(path);
    return info.fileName();
}

QString Utils::mapToString(QMap<QString, QString> map)
{
    QStringList valueList;
    for (QString key : map.keys())
    {
        QStringList temp;
        QString value = map.value(key);
        temp << key << value;
        valueList.append(temp.join(","));
    }

    return valueList.join(";");
}

QMap<QString, QString> Utils::stringToMap(QString value)
{
    QMap<QString, QString> map;
    QStringList valueList = value.split(";");
    for (QString one : valueList)
    {
        QStringList temp = one.split(",");
        map.insert(temp.at(0), temp.at(1));
    }

    return map;
}

SettingsParams Utils::tableParamsToSettingParams(TableParams params)
{
    //// 结构 field,fieldName,fieldType;....
    SettingsParams config;
    config.groupList.append(params.tableName);
    QStringList valueList;
    for (QString field : params.fieldList)
    {
        QString name = params.field_name_map.value(field);
        QString type = params.field_type_map.value(field);
        QStringList tempList;
        tempList << field << name << type;
        valueList.append(tempList.join(","));
    }
    config.settingsMap[params.tableName][TABLE_CONFIG_KEY] = valueList.join(";");

    return config;
}

TableParams Utils::settingsParamsToTableParams(SettingsParams params, QString tableName)
{
    TableParams tableConfig;
    tableConfig.tableName = tableName;
    QStringList valueList = params.settingsMap.value(tableName).value(TABLE_CONFIG_KEY).split(";");
    for (QString one : valueList)
    {
        QStringList tempList = one.split(",");
        tableConfig.addField(tempList.at(0),
                             tempList.at(1),
                             tempList.at(2));
    }

    return tableConfig;
}

