#include "dbtool.h"
#include <QDebug>
DbTool::DbTool(QObject *parent) : QObject(parent)
{

}

DbTool::~DbTool()
{
    for (QString key : connectManage.keys())
    {
        connectManage[key].close();
    }
    connectManage.clear();
}

DbTool &DbTool::getInstance()
{
    static DbTool tool;
    return tool;
}

void DbTool::setConnectParams(DbParams params)
{
    if (params.dbType == SQLITE)
    {
        if (!db.isOpen())
        {
            db = QSqlDatabase::addDatabase("QSQLITE", "MAIN");
            db.setDatabaseName(params.dbName);
            if (!db.open())
            {
                lastErrorInfo = "nomal connect open faild: " + db.lastError().text();
            }
        }
    }
}

bool DbTool::getOneDbConnect(DbParams &params)
{
    QSqlDatabase db;
    if (params.dbType == SQLITE)
    {
        // 判断请求的连接是否已打开
        if (connectManage.keys().contains(params.connectName))
        {
            params.db = connectManage[params.connectName];
            return true;
        }

        // 判断是否生成指定连接
        QString connectName;
        if (params.connectName.isEmpty())
        {
            connectName = keyTemp.arg(connectId++);
        }
        else
        {
            connectName = params.connectName;
        }
        db = QSqlDatabase::addDatabase("QSQLITE", connectName);
        db.setDatabaseName(params.dbName);

        // 判断是否打开
        if (db.isOpen())
        {
            params.db = db;
            return true;
        }

        // 未打开
        //  打开失败
        if (!db.open())
        {
            params.db = db;
            return false;
        }

        connectManage[connectName] = db;
        params.db = db;
        return true;
    }
    else if (params.dbType == MYSQL)
    {

    }
}

QStringList DbTool::getAllTableName()
{
    QStringList dataList;
//    // use db;
//    QString dbName = db.databaseName();
//    QString sql = QString(" use `%1` ").arg(dbName);

    // show tables;
    QString sql = " SELECT * FROM sqlite_master WHERE type='table'";
    QSqlQuery query(db);
    if (query.exec(sql))
    {
        while(query.next())
        {
            dataList.append(query.value(0).toString());
        }
    }
    else
    {
        lastErrorInfo = QString(" get all table name error %1, dbName = %2").arg(query.lastError().text()).arg(db.databaseName());
    }

    return dataList;
}

QString DbTool::getLastErrorInfo()
{
    return lastErrorInfo;
}

bool DbTool::creatTable(TableParams tableCreateParams)
{

    //// 获取连接，默认使用一个链接，来创建数据库
    DbParams params;
    params.dbType = SQLITE;
    params.connectName = "MAIN";
    if (!getOneDbConnect(params))
    {
        lastErrorInfo = " get connect error at DbTool::creatTable";
        return false;
    }

//    //// 测试
//    QSqlQuery query2(params.db);
//    QString sql2 = "CREATE TABLE `iffdb`  ("
//                   "`id` int(11) NOT NULL,"
//                   "`synFlag` varchar(10) NOT NULL,"
//                   "`version` tinyint(255)  NOT NULL,"
//                   "`endFlag` tinyint(255)  NOT NULL,"
//                   "`projectLenght` int(255) NOT NULL,"
//                   "`projectNO` smallint(5)  NOT NULL,"
//                   "`projectType` smallint(255) NOT NULL,"
//                   "`deviceID_Type` varchar(5)  NOT NULL,"
//                   "`deviceID_Postion` varchar(5)  NOT NULL,"
//                   "`deviceID_Uint` varchar(5)  NOT NULL,"
//                   "`deviceID_SysType` varchar(10)  NOT NULL,"
//                   "`deviceID_Code` varchar(10)  NOT NULL,"
//                   "`classification` tinyint(255)  NOT NULL,"
//                   "`projectReserved` varchar(8)  NOT NULL,"
//                   "`deviceCode` varchar(14)  NOT NULL,"
//                   "`lon` double NOT NULL,"
//                   "`lat` double NOT NULL,"
//                   "`alt` int(11) NOT NULL,"
//                   "`signalType` smallint(5)  NOT NULL,"
//                   "`scoutChannel` int(255) NOT NULL,"
//                   "`antennaOrigang` float NOT NULL,"
//                   "`antennaEndang` float NOT NULL,"
//                   "`antennaType` varchar(255)  NOT NULL,"
//                   "`antennaWave` varchar(255)  NOT NULL,"
//                   "`precision` varchar(255)  NOT NULL,"
//                   "`referrnceTime` bigint(20)  NOT NULL,"
//                   "`demodulationDataNum` int(11)  NOT NULL,"
//                   "`signalArrivalTime` bigint(20)  NOT NULL,"
//                   "`MSRatio` tinyint(4)  NOT NULL,"
//                   "`freq` int(255)  NOT NULL,"
//                   "`powPA` smallint(255) NOT NULL,"
//                   "`signalArrivalang` smallint(255)  NOT NULL,"
//                   "`modeCode` tinyint(255)  NOT NULL,"
//                   "`decodeLen` tinyint(255)  NOT NULL,"
//                   "`decodeData` varchar(260)  NOT NULL,"
//                   "`shakeValOfM5` varchar(255)  NOT NULL,"
//                   "`decodeDataOfM5` varchar(255)  NOT NULL,"
//                   "`time` datetime(0) NOT NULL)";
//    if (!query2.exec(sql2))
//    {
//        lastErrorInfo = QString(" query error : %1 \n sql is :").arg(query2.lastError().text()).arg(sql2);
//        return false;
//    }
//    return true;

    //// 创建数据表
    /// 第一个参数为 字段， 第二个参数为 type
    QString oneFieldTemp = " `%1` %2 ";
    QStringList contentList;
    for (QString field : tableCreateParams.fieldList)
    {
        QString type = tableCreateParams.getFieldType(field);
        contentList.append(oneFieldTemp.arg(field).arg(type));
    }

    /// 组装sql
    QString sql = QString(" CREATE TABLE `%1` ( %2 ) ").arg(tableCreateParams.tableName).arg(contentList.join(','));

    ///执行
    QSqlQuery query(params.db);
    if (!query.exec(sql))
    {
        lastErrorInfo = QString(" query error : %1 \n sql is :").arg(query.lastError().text()).arg(sql);
        return false;
    }

    return true;
}
